package DarkOrion.OS_kurs.lzw.io;

import java.io.IOException;
import java.io.OutputStream;

import DarkOrion.OS_kurs.lzw.util.Constants;
import DarkOrion.OS_kurs.lzw.util.Euclid;


public class Output {

	private OutputStream out;
	int codeWordLength;
	private int mask;
	private long buf; 
	private int written;
	private int bufUsageBits;
	private int bufUsageBytes;
	private int bufUsageSymbols;

	public Output(OutputStream out, int codeWordLength) {
		this.out = out;
		this.codeWordLength = codeWordLength;

		written = 0;
		buf = 0;
		bufUsageBits = (int) Euclid.LCM(Constants.BITS_IN_BYTE, codeWordLength);
		bufUsageBytes = bufUsageBits / Constants.BITS_IN_BYTE;
		bufUsageSymbols = bufUsageBits / codeWordLength;
		mask = (1 << codeWordLength) - 1;
	}


	public void write(int code) throws IOException {
		// Did you know, that Java is big-endian?
		code = (code & mask) << ((written) * codeWordLength);
		buf |= code;
		written++;
		if (written >= bufUsageSymbols) {
			for (int i = 0; i < bufUsageBytes; i++) {
				out.write((int) (buf & Constants.BYTE_MASK));
				buf >>= Constants.BITS_IN_BYTE;
			}
			written = 0;
			buf = 0;
		}
	}


	public void flush() throws IOException {
		while ((written < bufUsageSymbols) && (written != 0)) {
			write(-1);
		}
		out.flush();
	}


	public void close() throws IOException {
		out.close();
	}
}
