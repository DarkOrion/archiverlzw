package DarkOrion.OS_kurs.lzw.util;

public abstract class Constants {

	public static final int BITS_IN_BYTE = 8;

	public static final int BYTE_MASK = 255;

	public static final String COMPRESSED_FILE_SUFFIX = ".lzwj";
}
