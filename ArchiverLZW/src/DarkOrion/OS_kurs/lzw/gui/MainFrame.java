package DarkOrion.OS_kurs.lzw.gui;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;

import DarkOrion.OS_kurs.lzw.actions.CompressAction;
import DarkOrion.OS_kurs.lzw.actions.DecompressAction;


public class MainFrame extends JFrame {
	private static final int DEFAULT_WIDTH = 500;
	private static final int DEFAULT_HEIGHT = 100;
	private static final String DEFAULT_CAPTION = "LZW file compressor";

	private JButton btnCompress;
	private JButton btnDecompress;
	private Action compressAction;
	private Action decompressAction;

	public MainFrame() {
		compressAction = new CompressAction();
		decompressAction = new DecompressAction();
		btnCompress = new JButton(compressAction);
		btnDecompress = new JButton(decompressAction);

		this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		centerize();
		this.setResizable(false);
		this.setTitle(DEFAULT_CAPTION);
		this.setLayout(new FlowLayout());
		this.add(btnCompress);
		this.add(btnDecompress);

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


	private void centerize() {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		int screenWidth = toolkit.getScreenSize().width;
		int screenHeight = toolkit.getScreenSize().height;

		setLocation((screenWidth - getWidth()) / 2,
				(screenHeight - getHeight()) / 2);
	}
}
